# Assessment
FrontEnd:
==========
1. Framework used => ReactJs and Ant Design
2. Run npm install.
3. Default Login Credenials:
	User Name => demo
	Password  => demo
4. Idle Timeout Configured in env file (60 seconds)
5. Actions Implemented:
	1. Login Page
	2. Reset Password
	3. Idle Timeout

Backend API:
===========
1. Technology Used:
   1. Java Spring Boot 
   2. In Memory H2 DB
   3. Rest API
2. Port No configured in application.properties. (8090)
