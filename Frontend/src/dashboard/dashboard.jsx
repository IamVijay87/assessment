import { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Form, Input, Button, Row, Col, Modal, notification } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import axios from 'axios';
import '../../src/App.css';
import { useIdleTimer } from 'react-idle-timer';
import env from "react-dotenv";

function DashBoardComponent() {
    const [visible, setVisible] = useState();
    const [form] = Form.useForm();
    const history = useHistory();

    const transData = history.location.state;
    console.log({ transData });
    const logOut = () => {
        axios.post('http://localhost:8090/auth/session/destroy', { SessionId: transData.data }).then(function (response) {
            // handle success
            console.log(response);
            if (response.data != null && response.data.statusCode && response.data.statusCode === 200) {
                history.push(response.data.message);
            }

        })
            .catch(function (error) {
                console.log(error);
            });

    }

    const resetPassword = () => {
        setVisible(true);
    }


    const resetPasswordSubmit = (values) => {
        console.log('Received values of form: ', values);
        axios.post('http://localhost:8090/auth/resetpassword', {
            userName: values.userName,
            oldPassword: values.oldPassword,
            newPassword: values.newPassword,
            sessionId: transData.data
        }).then(function (response) {
            // handle success
            console.log(response);
            notification['success']({
                message: 'Reset Password',
                description:
                    'Reset Password successfully',
            });

        })
            .catch(function (error) {
                // handle error
                console.log(error);
            });
    }

    const onCancel = () => {
        setVisible(false);
    }

    const handleOnIdle = () => {
        notification['warning']({
            message: 'Session Timeout',
            description:
            'Page is Idle for ' + env.SESSION_IDLE_TIME_OUT/1000 + ' seconds.Session Timed out',
        });
        logOut();
    }

    const { getRemainingTime, getLastActiveTime } = useIdleTimer({
        timeout: env.SESSION_IDLE_TIME_OUT, //this is configured in .env file
        onIdle: handleOnIdle,
        //onActive: handleOnActive,
        //onAction: handleOnAction,
        debounce: 500,
        crossTab: {
            emitOnAllTabs: true
          }
      })

    return (
        <div className="Dashboard">
           
            <Row gutter={16}>
                <Col span={24}>
                    <h2>Welcome to Home Page</h2>
                </Col>
                <Col span={12}>
                    <Button className="App-header" type='link' onClick={resetPassword}>Reset Password</Button>
                </Col>

                <Col span={12}>
                    <Button className="App-header" type='link' onClick={logOut}>LogOut</Button>
                </Col>
            </Row>

            <Modal
                visible={visible}
                title="Reset Password"
                okText="Reset"
                cancelText="Cancel"
                onCancel={onCancel}
                onOk={() => {
                    form
                        .validateFields()
                        .then((values) => {
                            form.resetFields();
                            resetPasswordSubmit(values);
                        })
                        .catch((info) => {
                            console.log('Validate Failed:', info);
                        });
                }}
            >
                <Form
                    form={form}
                    layout="vertical"
                    name="form_in_modal"
                    initialValues={{
                        modifier: 'public',
                    }}
                >
                    <Form.Item
                        name="userName"
                        label="User Name"
                        rules={[
                            {
                                required: true,
                                message: 'Please input your Username!',
                            },
                        ]}
                        hasFeedback
                    >
                        <Input prefix={<UserOutlined />} placeholder="Username" />
                    </Form.Item>
                    <Form.Item
                        name="oldPassword"
                        label="Old Password"
                        rules={[
                            {
                                required: true,
                                message: 'Please input your Old Password!',
                            },
                        ]}
                        hasFeedback
                    >
                        <Input.Password
                            prefix={<LockOutlined />}
                            placeholder="Old Password"
                        />
                    </Form.Item>
                    <Form.Item
                        name="newPassword"
                        label="New Password"
                        rules={[
                            {
                                required: true,
                                message: 'Please input your new password!',
                            },
                            ({ getFieldValue }) => ({
                                validator(_, value) {
                                    if (!value || getFieldValue('oldPassword') !== value) {
                                        return Promise.resolve();
                                    }

                                    return Promise.reject(new Error('Old Password and New Password do not same!'));
                                },
                            }),
                        ]}
                        hasFeedback
                    >
                        <Input.Password prefix={<LockOutlined />}
                            placeholder="New Password"
                        />
                    </Form.Item>

                    <Form.Item
                        name="confirmPassword"
                        label="Confirm Password"
                        dependencies={['newPassword']}
                        hasFeedback
                        rules={[
                            {
                                required: true,
                                message: 'Please confirm your new password!',
                            },
                            ({ getFieldValue }) => ({
                                validator(_, value) {
                                    if (!value || getFieldValue('newPassword') === value) {
                                        return Promise.resolve();
                                    }

                                    return Promise.reject(new Error('The two passwords that you entered do not match!'));
                                },
                            }),
                            ({ getFieldValue }) => ({
                                validator(_, value) {
                                    if (!value || getFieldValue('oldPassword') !== value) {
                                        return Promise.resolve();
                                    }

                                    return Promise.reject(new Error('Old Password and New Password do not same!'));
                                },
                            }),
                        ]}
                    >
                        <Input.Password prefix={<LockOutlined />}
                            placeholder="Confirm New Password"
                        />
                    </Form.Item>
                </Form>
            </Modal>


        </div>
    );
}

export default DashBoardComponent;
