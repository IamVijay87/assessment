import React, { useState } from 'react';
import { useHistory } from "react-router-dom";
import 'antd/dist/antd.css';
import { Form, Input, Button, Card, Modal, notification } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import axios from 'axios';

const loginformstyle = { width: '400px', textAlign: 'center' };
const LoginComponent = () => {
    const [username, setUsername] = useState('');
    let history = useHistory();

    const onLogin = (values) => {
        console.log('Received values of form: ', values);
        axios.post('http://localhost:8090/auth/login', {
            userName: values.userName,
            password: values.password
        }).then(function (response) {
            // handle success
            console.log(response);
            if (response.data != null && response.data.statusCode && response.data.statusCode === 200) {
                history.push({ pathname: "/dashboard", state: { data: response.data.sessionId } });
            } else {
                notification['error']({
                    message: 'Login Response',
                    description:
                        'Login Failure. User Name or Password Incorrect.',
                });
            }
        })
            .catch(function (error) {
                console.log(error);
            });
    };




    return (
        <Card title="Login Form" style={loginformstyle}>
            <Form
                name="normal_login"
                initialValues={{
                    remember: true,
                }}
                onFinish={onLogin}
            >
                <Form.Item
                    name="userName"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your Username!',
                        },
                    ]}
                >
                    <Input prefix={<UserOutlined />} placeholder="Username" />
                </Form.Item>
                <Form.Item
                    name="password"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your Password!',
                        },
                    ]}
                >
                    <Input.Password
                        prefix={<LockOutlined />}
                        placeholder="Password"
                    />
                </Form.Item>
                
                <Form.Item>
                    <Button type="primary" htmlType="submit" className="login-form-button">Log in</Button>
                </Form.Item>
            </Form>
        </Card>
    );
};

export default LoginComponent;