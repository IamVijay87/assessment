import { BrowserRouter, Route, Switch,Redirect } from 'react-router-dom';
import './App.css';
import LoginComponent from './login/login';
import DashBoardComponent from './dashboard/dashboard';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <Route exact path="/" render={() => <Redirect to="/login" push />} />
          <Route path="/login" component={LoginComponent} />
          <Route path="/dashboard" component={DashBoardComponent} />
        </Switch>
      </BrowserRouter>

    </div>
  );
}

export default App;
