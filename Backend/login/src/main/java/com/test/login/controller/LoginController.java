package com.test.login.controller;

import com.test.login.Status;
import com.test.login.dto.ResetPassword;
import com.test.login.dto.ResponseMessage;
import com.test.login.dto.User;
import com.test.login.respository.UserRepository;
import com.test.login.service.LoginService;
import com.test.login.service.LoginServiceImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.SQLException;

@CrossOrigin(origins = "http://localhost:3000/", maxAge = 3600)
@RestController
public class LoginController {
    LoginService loginService;

    public LoginController(LoginService loginService) {
        this.loginService = loginService;
    }

    @PostMapping("auth/login")
    public ResponseEntity<ResponseMessage> Login(@RequestBody User user, HttpSession session){
        Status loginResponse = loginService.login(user);
        String sessionId = RequestContextHolder.currentRequestAttributes().getSessionId();
        ResponseMessage response = new ResponseMessage();
        if(loginResponse == Status.SUCCESS){
            response.SessionId = session.getId();
            response.StatusCode =HttpStatus.OK.value();
        }else{
            response.StatusCode =HttpStatus.UNAUTHORIZED.value();
        }
        return ResponseEntity.ok(response);

    }

    @PostMapping("auth/resetpassword")
    public ResponseEntity<ResponseMessage> ResetPassword(@RequestBody ResetPassword data) throws SQLException{
        Status resetResponse = loginService.resetPassword(data);
        ResponseMessage response = new ResponseMessage();
        if(resetResponse == Status.SUCCESS){
            response.StatusCode =HttpStatus.OK.value();
        }else{
            response.StatusCode =HttpStatus.UNAUTHORIZED.value();
        }
        return ResponseEntity.ok(response);
    }

    @PostMapping("/auth/session/destroy")
    public ResponseEntity<ResponseMessage> destroySession(@RequestBody ResponseMessage request) throws SQLException {
        String destroyResponse = loginService.destroySession(request.SessionId);
        ResponseMessage response = new ResponseMessage();
        response.Message = destroyResponse;
        response.StatusCode =HttpStatus.OK.value();
        return ResponseEntity.ok(response);
    }
}
