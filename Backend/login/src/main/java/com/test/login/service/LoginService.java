package com.test.login.service;

import com.test.login.Status;
import com.test.login.dto.ResetPassword;
import com.test.login.dto.User;
import org.springframework.stereotype.Component;

import java.sql.SQLException;

@Component
public interface LoginService {
    public Status login(User user);
    public Status resetPassword(ResetPassword data) throws SQLException;
    public String destroySession(String sessionId) throws SQLException;
    public Boolean isSessionValid(String sessionId) throws SQLException;
}
