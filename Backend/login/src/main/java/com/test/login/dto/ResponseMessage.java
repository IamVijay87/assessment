package com.test.login.dto;

import com.test.login.Status;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseMessage {
    public String Message;
    public int StatusCode;
    public String SessionId;
}
