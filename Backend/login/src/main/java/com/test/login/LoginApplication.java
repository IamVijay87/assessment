package com.test.login;

import com.test.login.dto.User;
import com.test.login.respository.UserRepository;
import com.test.login.service.LoginService;
import com.test.login.service.LoginServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LoginApplication {
	@Autowired
	private static UserRepository userRepository;

	public static void main(String[] args) {
		SpringApplication.run(LoginApplication.class, args);
	}

}
