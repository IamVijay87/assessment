package com.test.login;

public enum Status {
    SUCCESS,
    USER_ALREADY_EXISTS,
    FAILURE
}
