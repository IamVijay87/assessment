package com.test.login.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class User {
    public String userName;
    public String password;
}
