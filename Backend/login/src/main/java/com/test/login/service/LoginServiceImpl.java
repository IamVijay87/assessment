package com.test.login.service;

import com.test.login.Status;
import com.test.login.dto.ResetPassword;
import com.test.login.dto.User;
import com.test.login.entity.UserEntity;
import com.test.login.respository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class LoginServiceImpl implements LoginService{
    @Autowired
    UserRepository userRepository;

    public LoginServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Status login(User user) {
        if(userRepository.count() <= 0) {
            UserEntity userEntity = UserEntity.builder()
                    .userName("demo")
                    .password("demo")
                    .build();
            userEntity = userRepository.save(userEntity);
        }

        Iterable<UserEntity> users = userRepository.findAll();
        for (UserEntity userentity : users) {
            if (userentity.userName.equals(user.userName) && userentity.password.equals(user.password)) {
                return Status.SUCCESS;
            }
        }
        return Status.FAILURE;
    }

    @Override
    public Status resetPassword(ResetPassword data) throws SQLException {
        UserEntity userEntity = userRepository.findByUserNameAndPassword(data.userName, data.oldPassword);
        if(userEntity != null && isSessionValid(data.sessionId)) {
            userEntity.setPassword(data.newPassword);
            userRepository.save(userEntity);
            return Status.SUCCESS;
        }else{
            return Status.FAILURE;
        }
    }


    public String destroySession(String sessionId)
            throws SQLException {
        String sql = "DELETE FROM SPRING_SESSION WHERE SESSION_ID=" + "'" + sessionId + "'";
        Connection conn = DriverManager
                .getConnection("jdbc:h2:mem:testdb", "sa", "");
        Statement stat = conn.createStatement();
        int rows = stat.executeUpdate(sql);
        return rows > 0 ? "/login" : "/dashboard";
    }

    public Boolean isSessionValid(String sessionId)
            throws SQLException {
        String sql = "SELECT * FROM SPRING_SESSION WHERE SESSION_ID=" + "'" + sessionId + "'";
        Connection conn = DriverManager
                .getConnection("jdbc:h2:mem:testdb", "sa", "");
        Statement stat = conn.createStatement();
        ResultSet resultSet = stat.executeQuery(sql);
        resultSet.last();
        return resultSet.getRow() > 0;
    }
}
