package com.test.login.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResetPassword {
    public String userName;
    public String oldPassword;
    public String newPassword;
    public  String sessionId;
}
